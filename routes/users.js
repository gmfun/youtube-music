var express = require('express');
var router = express.Router();
var User = require('../model/user');
var Playlist = require('../model/playlist');

var passport = require('passport');
var FacebookStrategy = require('passport-facebook').Strategy;

var configAuth = require('../config/auth');

passport.serializeUser(function(user, done) {
  console.log(user, "searlised");
  done(null, user.id);
});

// used to deserialize the user
passport.deserializeUser(function(id, done) {
  User.findById(id, function(err, user) {
    done(err, user);
  });
});

passport.use(new FacebookStrategy({

      // pull in our app id and secret from our auth.js file
      clientID        : configAuth.facebookAuth.clientID,
      clientSecret    : configAuth.facebookAuth.clientSecret,
      callbackURL     : configAuth.facebookAuth.callbackURL

    },

    // facebook will send back the token and profile
    function(token, refreshToken, profile, done) {

      // asynchronous
      process.nextTick(function() {
        console.log(profile.id, "check profile");
        // find the user in the database based on their facebook id
        User.findOne({ 'facebook.id' : profile.id }, function(err, user) {
          // if there is an error, stop everything and return that
          // ie an error connecting to the database
            console.log("hrerere jana", user, err);
            if (err)
                return done(err);

          // if the user is found, then log them in
          if (user) {
              console.log("user has", user);
              return done(null, user); // user found, return that user
          } else {
              console.log("new user");
              // if there is no user found with that facebook id, create them
            var newUser            = new User();

            // set all of the facebook information in our user model
            newUser.facebook.id    = profile.id; // set the users facebook id
            newUser.facebook.token = token; // we will save the token that facebook provides to the user
            newUser.facebook.name  = profile.displayName; // look at the passport user profile to see how names are returned
            //newUser.facebook.email = profile.emails[0].value; // facebook can return multiple emails so we'll take the first
              console.log(newUser, "save user");
              // save our user to the database
            newUser.save(function(err) {
              if (err)
                throw err;

              // if successful, return the new user

              var playlist = new Playlist({
                  name: "favourite",
                  userId: newUser._id
              })
                playlist.save();
              return done(null, newUser);
            });
          }

        });
      });

    })
);


/* GET users listing. */
router.get('/', function(req, res, next) {
  res.send('respond with a resource');
});

router.get('/auth/facebook', passport.authenticate('facebook'), function(req, res) {

});

router.get('/auth/facebook/callback',
    passport.authenticate('facebook', { failureRedirect: '/' }),
    function(req, res) {
      res.redirect('/');
    });


//router.get('/auth/facebook/callback', passport.authenticate('facebook', {failureRedirect: '/'}, function(req, res) {
//  console.log(res, req);
//  res.redirect('/')
//}))

module.exports = router;
