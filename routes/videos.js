var express = require('express');
var router = express.Router();
var Video = require('../model/video');
var User = require('../model/user');

router.post("/viewed", function(req, res, next) {
    var video = req.body.videoId;
    //console.log(req.body, "body post");
    //console.log(video);
    //res.send(req.body);

    if(req.user) {

    }else {
        Video.findOne({videoId: video.id.videoId}, function(err, data) {
            if(err) {

            }
            if(data) {
                data.count = data.count + 1;
                data.save().then(function(view) {
                    res.send(view)
                })
            }else {
                //console.log(video.snippet);
                var view = new Video;
                view.videoId = video.id.videoId;
                view.snippet = {
                    title: video.snippet.title,
                    thumbnails: {
                        default: {
                            url: video.snippet.thumbnails.default.url
                        }
                    }
                };
                view.save().then(function(view) {
                    res.send(view)
                })
            }
        });
    }


});

router.use("/suggested", function(req, res) {
    console.log("check user", req.user);
    if(req.user){
        console.log("hasd user");
        Video.find({userId: req.user.id}).sort({"count": 1}).limit(10).then(function(video) {
            console.log("find video", video);
            res.send(video)
        })
    }
    else {
        Video.find().sort({"count": -1}).limit(10).then(function(video) {
            res.send(video)
        })
    }

});

module.exports = router;