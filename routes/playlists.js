var express = require('express');
var router = express.Router();
var Playlist = require('../model/playlist');
var User = require('../model/user');

var ObjectId = require('mongodb').ObjectID;



router.get("/", function (req, res) {
    if(req.user){
        Playlist.find({userId: req.user._id}).then(function(playlist){
            res.send(playlist)
        })
    }else {
        res.send(504)
    }

});

router.get("/:id", function (req, res) {
    console.log(req.params);
    Playlist.findOne({'_id': req.params.id}).then(function(playlist){
        console.log("herrer", playlist.share_link);
        if(req.user){
            if(playlist.userId == req.user._id) res.send(playlist);
        }

        else if(playlist.share_link) {
            //var obj = {
            //    videos: playlist.videos,
            //    name: playlist.name,
            //    _id: playlist._id
            //}
            console.log("and");
            res.render("sharable", {playlist: JSON.stringify(playlist)})
        }
    })
});

router.post("/", function(req, res) {
    if(req.user){
        var data = req.body;
        var playlist = new Playlist({
            name: data.name,
            userId: req.user._id
        });
        if(data.videos){
            playlist.videos = data.videos
        }
        playlist.save().then(function(playlist) {
            res.send(playlist)
        })
    }else {
        res.sendStatus(504)
    }

});

router.post("/add/:id", function(req, res) {
    console.log(req.body, req.params.id);
    //if(!Array.isArray(req.body.video)) req.body.video = [req.body.video];
    //Playlist.update(
    //    { '_id': req.params.id },
    //    {
    //        $addToSet: {
    //            "videos": req.body.video
    //        }
    //    }
    //).then(function(playlist){
    //        res.end(playlist)
    //    });
    Playlist.findById(req.params.id).then(function(playlist){
        var obj = req.body.video;
        obj.videoId = obj.id.videoId;
        playlist.videos.push(req.body.video)
        playlist.save().then(function(playlist) {
            res.send(req.body.video)
        })
    })
});

router.post("/remove/:id", function (req, res) {
    Playlist.update(
        { _id: req.params.id },
        { $pull: { 'videos': { videoId: req.body.videoId } } }
    ).then(function(){
            res.status(200)
        });

});

router.post("/current-playlist", function(req, res) {
    User.findById(req.user._id).then(function(user) {
        user.current_playlist = req.body.playlistId;
        user.save().then(function(){
            res.send({"current_playlist": user.current_playlist});
        }

        )
    })
});

//router.post("/current_playlist", function(req, res){
//    User.findById(req.body.playlist.userId).then(function(user) {
//        user.current_playlist = req.body.playlist._id;
//        user.save()
//    })
//})

router.post("/favourite/:id", function (req, res) {
    if(!Array.isArray(req.body.video)) req.body.video = [req.body.video]
    Playlist.update(
        { name: 'favorite' },
        { $addToSet: {videos: {
            $each: req.body.video
        } } }
    ).then(function(playlist){
            res.status(200)
        });
})

router.post("/share", function(req, res) {
    Playlist.findById(req.body.playlistId).then(function(playlist) {
        playlist.share_link = "/playlists/" + playlist._id;
        playlist.save().then(function(){
            res.sendStatus(200)
        })
    })
});

router.get("/delete/:id", function(req, res) {
    Playlist.findById(req.params.id).then(function(playlist) {
        console.log("checksdsdsds", req.user._id, playlist.userId, req.user._id._id == playlist.userId._id, typeof(playlist.userId) );
        if(req.user._id._id == playlist.userId._id) {
            console.log("removing");
            playlist.remove(function() {
                console.log("removed");
                res.send(playlist._id)
            })
        } else {
            console.log("or not");
            res.send(400)
        }
        //res.end()
    });

    //Playlist.find({_id: req.params.id}).remove(function() {
    //    res.status(200);
    //})
});

module.exports = router;
