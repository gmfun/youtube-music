var mongoose     = require('mongoose'),
    Schema = mongoose.Schema,
    searchPlugin = require('mongoose-search-plugin');

var SearchSchema   = new Schema({
    key: {type: String, index: true},
    count: {type: Number, default: 1}
});

SearchSchema.index({ key: 'text'});

SearchSchema.plugin(searchPlugin, {
    fields: ['key', 'videoName']
});

//SearchSchema.plugin(textSearch({
//    fields: ['key']
//}));


module.exports =  mongoose.model('Search', SearchSchema);