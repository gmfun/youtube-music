var mongoose     = require('mongoose'),
    Schema = mongoose.Schema,
    ObjectId = Schema.ObjectId,
    timestamps = require('mongoose-timestamp');

var VideoSchema   = new Schema({
    videoId: {type: String, index: true},
    count: {type: Number, default: 1},
    userId:  ObjectId,
    snippet: {
        title: String,
        thumbnails: {
            default: {
                url: String
            }
        }
    },
    next: [{
        videoId: String,
        count: Number,
        snippet: {
            title: String,
            thumbnails: {
                default: {
                    url: String
                }
            }
        },
    }]
});

VideoSchema.plugin(timestamps);

module.exports = mongoose.model('Video', VideoSchema);