var mongoose     = require('mongoose'),
    Schema = mongoose.Schema,
    ObjectId = Schema.Types.ObjectId,
    timestamps = require('mongoose-timestamp');

var PlaylistSchema   = new Schema({
    name: {type: String},
    sharable: Boolean,
    userId: ObjectId,
    share_link: String,
    current_index: {type: Number, default: -1},
    videos: [
        {
            videoId: {type: String, index: true},
            snippet: {
                title: String,
                thumbnails: {
                    default: {
                        url: String
                    }
                }
            }
        }
    ]
});

PlaylistSchema.plugin(timestamps);

module.exports = mongoose.model('Playlist', PlaylistSchema);
//module.exports = {};