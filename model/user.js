var mongoose     = require('mongoose'),
    Schema = mongoose.Schema,
    ObjectId = Schema.Types.ObjectId,
    textSearch = require('mongoose-search-plugin');
var timestamps = require('mongoose-timestamp');


var UserSchema   = new Schema({
    username: String,
    current_playlist: ObjectId,
    //passwdHash: { type: String, required: true },
    facebook: {
        id: String,
        token: String,
        email: String,
        name: String
    },
    search: [{
        key: {type: String},
        count: {type: Number, default: 1}
    }]
});

UserSchema.plugin(timestamps);

//UserSchema.index({ 'search.key': 'text' });



module.exports = mongoose.model('User', UserSchema);