var share = angular.module("youtubeShare", ['btford.socket-io'])
    
share.factory("playlistService", ["$http", function($http) {
    return {
        create: function(obj){
            return $http.post("/playlists/", obj)
        },
        add: function(obj, id){
            return $http.post("/playlists/add/" + id, {video: obj})
        },
        index: function(){
            return $http.get("/playlists")
        }
    }
}])  ;

share.factory("searchService", ["$http", function($http) {
    return {
        get: function(query) {
            var query  = encodeURI(query);
            return $http.get("https://www.googleapis.com/youtube/v3/search?part=snippet&maxResults=6&q=" + query +"&key=" + API_KEY)
        },
        related: function(id) {
            return $http.get("https://www.googleapis.com/youtube/v3/search?part=snippet&relatedToVideoId=" + id + "&type=video&key=" + API_KEY)
        },
        post: function(query) {
            return $http.post("/add-key", {key: query})
        },
        autocomplete: function(query) {
            return $http.jsonp("http://suggestqueries.google.com/complete/search?hl=en&ds=yt&client=youtube&hjson=t&cp=1&q="+query+"&key="+API_KEY+"&format=5&alt=json&callback=JSON_CALLBACK")
        }
    }
}]);

share.factory('socket', function (socketFactory) {
    var myIoSocket = io.connect(BASE_URL);

    var socket = socketFactory({
        ioSocket: myIoSocket
    });

    return socket;
});
    
share.controller("shareCtrl", ["$http", "$scope", "playlistService", "searchService", "socket", function ($http, $scope, playlistService, searchService, socket) {
    //$scope.playlist = JSON.parse($scope.playlist)
    //console.log(angular.element("#playlist-server"));

    $scope.suggestedIndex = -1;
    //var socket = io();
    //
    angular.element().ready(function(){
        //console.log($("#playlistserver").text(), "dsdads");
        console.log(JSON.parse($("#playlistserver").text()));
        console.log("dsadas");
        $scope.playlist = JSON.parse($("#playlistserver").text());
        socket.emit('join', {id: $scope.playlist._id})
        $scope.$apply()
    });

    //$(window).one("focus", );
    //$(function() {s
    //    var socket = io('http://localhost');
    //    socket.on('news', function (data) {
    //        console.log(data);
    //        socket.emit('my other event', { my: 'data' });
    //    });
    //})

    $scope.search = function(query) {
        $scope.showSearched = true;
        if($scope.suggestedIndex == -1){
            console.log(query, "query");

        }else {
            query = $scope.suggesteds[$scope.suggestedIndex][0]
        }
        searchService.get(query).then(function(res) {
            console.log(res);
            $scope.searchList = res.data.items
        });
        searchService.post($scope.query).then(function(){
            console.log("search query successfully posted");
        });
        $scope.query = "";
        //console.log("asdasd", query);

    };
    $scope.keypress = function(event){
        //console.log(event);
        if(event.keyCode == 40) {
            if($scope.suggestedIndex < $scope.suggesteds.length-1) $scope.suggestedIndex++
        }
        if(event.keyCode == 38) {
            if($scope.suggestedIndex != -1){
                $scope.suggestedIndex--
            }
        }
        //if(event.keyCode == 13) {
        //    if($scope.suggestedIndex != -1){
        //        console.log("hrere", $scope.suggesteds[$scope.suggestedIndex][0]);
        //        //console.log($scope.suggesteds[$scope.suggestedIndex]);
        //        $scope.selectSuggested($scope.suggesteds[$scope.suggestedIndex][0])
        //        event.preventDefault();
        //    }
        //
        //}
    };
    $scope.$watch("query", function(current, old) {
        if(current) {
            $scope.autocomplete()
        }
        else{
            $scope.suggesteds = false;
            $scope.suggestedIndex = -1;
        }
    });
    $scope.autocomplete = function() {
        //$http.jsonp("http://suggestqueries.google.com/complete/search?hl=en&ds=yt&client=youtube&hjson=t&cp=1&q="+$scope.query+"&key="+API_KEY+"&format=5&alt=json").then(function(res) {
        //    console.log(res, "success");
        //})
        searchService.autocomplete($scope.query).then(function(res) {
            $scope.suggesteds = res.data[1]
        });
        //$http.get("/suggestion?key=" + $scope.query).then(function(res) {
        //    $scope.suggesteds = res.data
        //})
    };

    $scope.selectSuggested = function(search) {
        console.log(search, "search");
        $scope.query = search;
        $scope.suggestedIndex = -1;
        $scope.search(search);
        //$scope.query = "";
    }

    $scope.addToPlaylist = function (video, callback) {
        //console.log($scope.playlist, $scope.playlists[$scope.playlist.playlistIndex].videos);
        //$scope.playlist.videos.push(video);
        //$scope.playlists[$scope.playlist.playlistIndex].videos.push(video)
        playlistService.add(video, $scope.playlist._id).then(function(res) {
            $scope.playlist.videos.push(res.data);
            if(callback) callback(res.data);
            $scope.showSearched = false;
            socket.emit('addToPlaylist', {playlistId: $scope.playlist._id, video: res.data})
            //$scope.playlists[$scope.playlist.playlistIndex].videos.push(video)
        }).catch(function(err){
            console.log(err, "err");
        })
    }
    
    $scope.addAndPlayNext = function(video) {
        $scope.addToPlaylist(video, function(video){
            //play now use socket.io
        })
    };

    $scope.playNext = function(index){
        socket.emit("updateIndex", {
            playlistId: $scope.playlist._id,
            index: index
        })
        $scope.playlist.current_index  = index;
    };

    $scope.selectSuggested = function(index) {
        $scope.suggestedIndex = -1;
        $scope.search($scope.suggesteds[index][0])
    }

    socket.on("videoAdded", function(data) {
        //console.log("reviced video", data);
        //alert(data)
        data.videoId = data.id.videoId;
        $scope.playlist.videos.push(data)
    })

    socket.on("updateIndex", function(data) {
        console.log("updateindex", data);
        $scope.playlist.current_index  = data.index;
    })

    socket.on("videoDeleted", function(data) {
        $scope.playlist.videos = $scope.playlist.videos.splice(data.index, 0);
    })

}]);