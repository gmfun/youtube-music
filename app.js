var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var session = require('express-session');

var mongoose   = require('mongoose');

var routes = require('./routes/index');
var users = require('./routes/users');
var videos = require('./routes/videos');
var playlists = require('./routes/playlists');

var passport = require('passport');

var env = require('./config/env');

var app = express();
//var server = require('http').createServer(app);
// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

mongoose.connect("mongodb://gmfun:gmfun@ds031912.mongolab.com:31912/heroku_t4mnxwsk");



// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

// app.use(session({
//   secret: 'my_precious' ,
//   resave: false,
//   saveUninitialized: false
// }));
app.use(passport.initialize());
app.use(passport.session());
app.use(function(req, res, next) {
    res.locals.env = process.env.NODE_ENV;
    res.locals.BASE_URL = env.BASE_URL
    //  next({env : process.env.NODE_ENV;})
    next()
})

app.use('/users', users);

app.use('/videos', videos);
app.use('/playlists', playlists);

app.use('/', routes);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});


// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  });
});




app.use(function(err, req, res, next) {
  var request = http.get("https://www.googleapis.com/oauth2/v1/tokeninfo?access_token=ACCESS_TOKEN", function(response) {
    response.on()
  });
  next()
})




module.exports = app;
