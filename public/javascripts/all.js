var share = angular.module("youtubeShare", ['btford.socket-io'])
    
share.factory("playlistService", ["$http", function($http) {
    return {
        create: function(obj){
            return $http.post("/playlists/", obj)
        },
        add: function(obj, id){
            return $http.post("/playlists/add/" + id, {video: obj})
        },
        index: function(){
            return $http.get("/playlists")
        }
    }
}])  ;

share.factory("searchService", ["$http", function($http) {
    return {
        get: function(query) {
            var query  = encodeURI(query);
            return $http.get("https://www.googleapis.com/youtube/v3/search?part=snippet&maxResults=6&q=" + query +"&key=" + API_KEY)
        },
        related: function(id) {
            return $http.get("https://www.googleapis.com/youtube/v3/search?part=snippet&relatedToVideoId=" + id + "&type=video&key=" + API_KEY)
        },
        post: function(query) {
            return $http.post("/add-key", {key: query})
        },
        autocomplete: function(query) {
            return $http.jsonp("http://suggestqueries.google.com/complete/search?hl=en&ds=yt&client=youtube&hjson=t&cp=1&q="+query+"&key="+API_KEY+"&format=5&alt=json&callback=JSON_CALLBACK")
        }
    }
}]);

share.factory('socket', function (socketFactory) {
    var myIoSocket = io.connect(BASE_URL);

    var socket = socketFactory({
        ioSocket: myIoSocket
    });

    return socket;
});
    
share.controller("shareCtrl", ["$http", "$scope", "playlistService", "searchService", "socket", function ($http, $scope, playlistService, searchService, socket) {
    //$scope.playlist = JSON.parse($scope.playlist)
    //console.log(angular.element("#playlist-server"));

    $scope.suggestedIndex = -1;
    //var socket = io();
    //
    angular.element().ready(function(){
        //console.log($("#playlistserver").text(), "dsdads");
        console.log(JSON.parse($("#playlistserver").text()));
        console.log("dsadas");
        $scope.playlist = JSON.parse($("#playlistserver").text());
        socket.emit('join', {id: $scope.playlist._id})
        $scope.$apply()
    });

    //$(window).one("focus", );
    //$(function() {s
    //    var socket = io('http://localhost');
    //    socket.on('news', function (data) {
    //        console.log(data);
    //        socket.emit('my other event', { my: 'data' });
    //    });
    //})

    $scope.search = function(query) {
        $scope.showSearched = true;
        if($scope.suggestedIndex == -1){
            console.log(query, "query");

        }else {
            query = $scope.suggesteds[$scope.suggestedIndex][0]
        }
        searchService.get(query).then(function(res) {
            console.log(res);
            $scope.searchList = res.data.items
        });
        searchService.post($scope.query).then(function(){
            console.log("search query successfully posted");
        });
        $scope.query = "";
        //console.log("asdasd", query);

    };
    $scope.keypress = function(event){
        //console.log(event);
        if(event.keyCode == 40) {
            if($scope.suggestedIndex < $scope.suggesteds.length-1) $scope.suggestedIndex++
        }
        if(event.keyCode == 38) {
            if($scope.suggestedIndex != -1){
                $scope.suggestedIndex--
            }
        }
        //if(event.keyCode == 13) {
        //    if($scope.suggestedIndex != -1){
        //        console.log("hrere", $scope.suggesteds[$scope.suggestedIndex][0]);
        //        //console.log($scope.suggesteds[$scope.suggestedIndex]);
        //        $scope.selectSuggested($scope.suggesteds[$scope.suggestedIndex][0])
        //        event.preventDefault();
        //    }
        //
        //}
    };
    $scope.$watch("query", function(current, old) {
        if(current) {
            $scope.autocomplete()
        }
        else{
            $scope.suggesteds = false;
            $scope.suggestedIndex = -1;
        }
    });
    $scope.autocomplete = function() {
        //$http.jsonp("http://suggestqueries.google.com/complete/search?hl=en&ds=yt&client=youtube&hjson=t&cp=1&q="+$scope.query+"&key="+API_KEY+"&format=5&alt=json").then(function(res) {
        //    console.log(res, "success");
        //})
        searchService.autocomplete($scope.query).then(function(res) {
            $scope.suggesteds = res.data[1]
        });
        //$http.get("/suggestion?key=" + $scope.query).then(function(res) {
        //    $scope.suggesteds = res.data
        //})
    };

    $scope.selectSuggested = function(search) {
        console.log(search, "search");
        $scope.query = search;
        $scope.suggestedIndex = -1;
        $scope.search(search);
        //$scope.query = "";
    }

    $scope.addToPlaylist = function (video, callback) {
        //console.log($scope.playlist, $scope.playlists[$scope.playlist.playlistIndex].videos);
        //$scope.playlist.videos.push(video);
        //$scope.playlists[$scope.playlist.playlistIndex].videos.push(video)
        playlistService.add(video, $scope.playlist._id).then(function(res) {
            $scope.playlist.videos.push(res.data);
            if(callback) callback(res.data);
            $scope.showSearched = false;
            socket.emit('addToPlaylist', {playlistId: $scope.playlist._id, video: res.data})
            //$scope.playlists[$scope.playlist.playlistIndex].videos.push(video)
        }).catch(function(err){
            console.log(err, "err");
        })
    }
    
    $scope.addAndPlayNext = function(video) {
        $scope.addToPlaylist(video, function(video){
            //play now use socket.io
        })
    };

    $scope.playNext = function(index){
        socket.emit("updateIndex", {
            playlistId: $scope.playlist._id,
            index: index
        })
        $scope.playlist.current_index  = index;
    };

    $scope.selectSuggested = function(index) {
        $scope.suggestedIndex = -1;
        $scope.search($scope.suggesteds[index][0])
    }

    socket.on("videoAdded", function(data) {
        //console.log("reviced video", data);
        //alert(data)
        data.videoId = data.id.videoId;
        $scope.playlist.videos.push(data)
    })

    socket.on("updateIndex", function(data) {
        console.log("updateindex", data);
        $scope.playlist.current_index  = data.index;
    })

    socket.on("videoDeleted", function(data) {
        $scope.playlist.videos = $scope.playlist.videos.splice(data.index, 0);
    })

}]);
var player;

//function onYouTubeIframeAPIReady() {
//    player = new YT.Player('player', {
//        height: '390',
//        width: '640',
//        videoId: 'y6Sxv-sUYtM'
//    });
//    console.log(player);
//}
angular.module("youtube", ['youtube-embed', 'btford.socket-io']);

angular.module("youtube").factory("searchService", ["$http", function($http) {
    return {
        get: function(query) {
            var query  = encodeURI(query);
            return $http.get("https://www.googleapis.com/youtube/v3/search?part=snippet&maxResults=6&q=" + query +"&key=" + API_KEY)
        },
        related: function(id) {
            return $http.get("https://www.googleapis.com/youtube/v3/search?part=snippet&maxResults=9&relatedToVideoId=" + id + "&type=video&key=" + API_KEY)
        },
        post: function(query) {
            return $http.post("/add-key", {key: query})
        },
        autocomplete: function(query) {
            return $http.jsonp("https://suggestqueries.google.com/complete/search?hl=en&ds=yt&client=youtube&hjson=t&cp=1&q="+query+"&key="+API_KEY+"&format=5&alt=json&callback=JSON_CALLBACK")
        }
    }
}]);

angular.module("youtube").factory("playerService", [function() {
    //console.log("log", player);

    return {
        get: function() {
            return player
        },
        play: function(id) {
            return player.loadVideoById(id)
        }
    }
}]);

angular.module("youtube").factory('socket', function (socketFactory) {
    var myIoSocket = io.connect();

    var socket = socketFactory({
        ioSocket: myIoSocket
    });

    return socket;
});

angular.module("youtube").factory("playlistService", ["$http", "socket", function($http, socket) {
    return {
        create: function(obj){
            return $http.post("/playlists/", obj)
        },
        add: function(obj, id){
            return $http.post("/playlists/add/" + id, {video: obj})
        },
        index: function(){
            return $http.get("/playlists")
        },
        share: function(id) {
            return $http.post( "/playlists/share" ,{playlistId: id})
        },
        updateIndex: function(playlistId, index){
            socket.emit("updateIndex", {
                playlistId: playlistId,
                index: index
            })
        },
        current_playlist: function(playlistId){
            var obj = {playlistId: playlistId};
            return $http.post( "/playlists/current-playlist", obj)
        },
        deletePlaylist: function(id) {
            return $http.get("/playlists/delete/" + id)
        }
    }
}])

angular.module("youtube").factory("backendService", ["$http", function($http) {
    return {
        viewed: function(video) {
            console.log(video, "Service");
            return $http.post("/videos/viewed", {videoId: video})
        },
        suggested: function(){
            return $http.get("/videos/suggested")
        }
    }
}])

angular.module("youtube").controller("youtubeCtrl", ["$scope", "searchService",  "playerService", "$http", "backendService", "playlistService", "socket", function($scope, searchService, playerService, $http, backendService, playlistService, socket) {
    $scope.baseUrl = BASE_URL;
    console.log(BASE_URL);
    $scope.nowPlaying = [];
    $scope.playlists = [];
    $scope.newPlaylist = {};
    $scope.video = {id: {
        videoId: 'y6Sxv-sUYtM'
    }};
    $scope.preferance = preferance;
    $scope.user = user;
    console.log(user,"user");
    var start = true;

    $scope.suggestedIndex = -1;
    $scope.search = function(query) {
        $scope.showSearched = true;
        if($scope.suggestedIndex == -1){
            console.log(query, "query");

        }else {
            query = $scope.suggesteds[$scope.suggestedIndex][0]
        }
        searchService.get(query).then(function(res) {
            console.log(res);
            $scope.searchList = res.data.items
        });
        searchService.post($scope.query).then(function(){
            console.log("search query successfully posted");
        });
        $scope.query = "";
        //console.log("asdasd", query);

    };

    $scope.test = function(str) {
        //console.log($scope.str.key);
        $http.get("/suggestion?key=" + $scope.str.key).then(function(res) {
            console.log(res);
        })
    };

    $scope.$watch("query", function(current, old) {
        if(current) {
            $scope.autocomplete()
        }
        else{
            $scope.suggesteds = false;
            $scope.suggestedIndex = -1;
        }
    });
    $scope.updateRecommendation = function() {
        backendService.suggested().then(function(res) {
            res.data.forEach(function(video) {
                video.id = {videoId: video.videoId}
            });
            $scope.recommendations = res.data
        });
    }

    $scope.updateRecommendation();

    playlistService.index().then(function(res){
        $scope.playlists = res.data;
        var playlist = false;
        $scope.playlists.forEach(function(thisplaylist){
            if(thisplaylist._id == current_playlist){
                playlist = thisplaylist;
                return
            }
        });
        console.log(playlist, "playlist", current_playlist);
        if(!playlist){
            console.log("no playlist");
            playlist = res.data[0];
        }
        $scope.selectPlaylist(playlist, 0);
    });


    $scope.autocomplete = function() {
        //$http.jsonp("http://suggestqueries.google.com/complete/search?hl=en&ds=yt&client=youtube&hjson=t&cp=1&q="+$scope.query+"&key="+API_KEY+"&format=5&alt=json").then(function(res) {
        //    console.log(res, "success");
        //})
        searchService.autocomplete($scope.query).then(function(res) {
            $scope.suggesteds = res.data[1]
        });
        //$http.get("/suggestion?key=" + $scope.query).then(function(res) {
        //    $scope.suggesteds = res.data
        //})
    };

    $scope.keypress = function(event){
        //console.log(event);
        if(event.keyCode == 40) {
            if($scope.suggestedIndex < $scope.suggesteds.length-1) $scope.suggestedIndex++
        }
        if(event.keyCode == 38) {
            if($scope.suggestedIndex != -1){
                $scope.suggestedIndex--
            }
        }
        //if(event.keyCode == 13) {
        //    if($scope.suggestedIndex != -1){
        //        event.preventDefault();
        //        //console.log($scope.suggesteds[$scope.suggestedIndex]);
        //        $scope.selectSuggested($scope.suggesteds[$scope.suggestedIndex][0])
        //    }
        //
        //}
    };

    $scope.selectSuggested = function(search) {
        console.log(search);
        $scope.query = search;
        $scope.suggestedIndex = -1;
        $scope.search(search);
        //$scope.query = "";
    }

    $scope.selectFromPlaylist = function(video, index) {
        console.log("herer", index);
        $scope.playVideo(video);
        if(Number.isInteger(index)) $scope.nowPlaying.splice(index, 1)
    }
    var playnext;

    $scope.selectVideo = function(video, playlistIndex) {
        if(!video.id) video.id = {videoId: video.videoId};
        $scope.showSearched = false;
        console.log("playlist", playlistIndex);
        if(Number.isInteger(playlistIndex)){
            $scope.selectedPlaylist.current_index = playlistIndex;
            playlistService.updateIndex($scope.selectedPlaylist._id, playlistIndex);

        }
        $scope.nowPlaying.push(angular.copy(video));
        if(start){
            console.log("dtsat");
            $scope.playNext();
            $scope.$on("youtube.player.ready", function($event, player) {
                player.playVideo();
            });
            start = false
        }


        //playerService.get().addEventListener("onStateChange", function(event) {
        //    console.log(event);
        //    if(event.data == 0){
        //        if(playnext) clearTimeout(playnext)
        //        playnext = setTimeout($scope.playNext(), 500)
        //        //$scope.playNext()
        //    }
        //});



        //if(start) {
        //    $scope.playVideo(video);
        //    $scope.$on("youtube.player.ready", function($event, player) {
        //        player.playVideo();
        //    });
        //    $scope.nowPlaying = [];
        //    start = false
        //}
    };
    $scope.$on('youtube.player.ended', function ($event, player) {
        console.log("eneded");
        console.log($scope.nowPlaying, "1");

    });

    function getReadyPlayer() {
        player.addEventListener('onStateChange', function(event){
            console.log("event", event);
            if (event.data == YT.PlayerState.ENDED) {
                console.log("song ended");
                $scope.playNext();
                player.addEventListener('onReady', function(){
                    console.log("ready player");
                    player.playVideo()
                })
            }
        });
        //if(!player){
        //    setTimeout(getReadyPlayer, 300)
        //}else {
        //}
    }
    promise.then(function(data){
        console.log(player, "resolved");
        getReadyPlayer();
    });


    $scope.$on('youtube.player.playing', function ($event, player) {
        start = false;
    });


    var discoverCount1 = 0;
    var discoverCount2 = 0;

    $scope.playNext = function() {
        start = false;
        if($scope.nowPlaying.length > 0) {
            $scope.playVideo($scope.nowPlaying.shift());
        } else {
            if($scope.preferance == -1) {
                if(discoverCount1 == 7){
                    $scope.playNextFromPlaylist();
                    discoverCount1 = 0
                } else {
                    var i = Math.floor(Math.random()*3);
                    $scope.playVideo($scope.relatedVideos[i]);
                    discoverCount1++;
                }
            } else  if($scope.preferance == 0) {
                if(discoverCount2 == 2) {
                    $scope.playNextFromPlaylist(true);
                    discoverCount2 = 0
                } else {
                    var index = Math.floor(Math.random()*3);

                    $scope.playVideo($scope.relatedVideos[index]);
                    discoverCount2++;
                }

            } else {
                $scope.playNextFromPlaylist()
            }
        }


    };

    $scope.playNextFromPlaylist = function(random) {
        if($scope.selectedPlaylist && $scope.selectedPlaylist.videos.length > 0) {
            if(random) {
                var index = getRandomInt(0, $scope.selectedPlaylist.videos.length - 1);
                $scope.selectedPlaylist.current_index = index;
            } else {
                if($scope.selectedPlaylist.current_index < $scope.selectedPlaylist.videos.length - 1){
                    $scope.selectedPlaylist.current_index = $scope.selectedPlaylist.current_index + 1;
                }else if($scope.selectedPlaylist.videos.length > 10) {
                    $scope.selectedPlaylist.current_index = 0;
                    $scope.playVideo($scope.relatedVideos[0]);
                    return true;
                } else {
                    $scope.selectedPlaylist.current_index = 0;
                    $scope.playVideo($scope.relatedVideos[0]);
                    $scope.preferance = 0;
                    return true;
                }
            }

            playlistService.updateIndex($scope.selectedPlaylist._id, $scope.selectedPlaylist.current_index);
            video = $scope.selectedPlaylist.videos[$scope.selectedPlaylist.current_index];
            video.id = {videoId: video.videoId};
            $scope.playVideo(video);
        } else {
            var index = Math.floor(Math.random()*3);
            $scope.playVideo($scope.relatedVideos[index]);
        }

    };

    $scope.addOnTop = function(video) {
        $scope.showSearched = false;
        $scope.nowPlaying.splice(0, 0, video)
    };

    $scope.playVideoFromPlaylist = function(video, index) {
        $scope.playVideo(video);
        $scope.selectedPlaylist.current_index = index;
    };

    $scope.playVideo = function(video) {
        console.log(video, "recieve");
        var videoId = video.id ? video.id.videoId : video.videoId;
        $scope.showSearched = false;
        player.loadVideoById(videoId);
        //$scope.video = video;
        backendService.viewed(video).then(function(res) {
            console.log(res.data, video, "see");
        });
        $scope.updateRecommendation();
        searchService.related(getId(video)).then(function(res) {
            $scope.relatedVideos = res.data.items
        })
    };

    $scope.removeFromList = function(index) {
        $scope.nowPlaying.splice(index, 1)
    };

    $scope.createPlaylist = function(){
        console.log($scope.newPlaylist);

        playlistService.create($scope.newPlaylist).then(function(res) {
            $scope.newPlaylist = {};
            console.log(res);
            $scope.playlists.push(res.data)
        })
    };
    $scope.selectPlaylist = function (playlist, index) {
        console.log(playlist);

        if(playlist.videos.length > 2 && $scope.preferance == -1) $scope.preferance = 1;
        playlist.playlistIndex = index;
        $scope.selectedPlaylist = playlist;
        socket.emit("join", {id: playlist._id});
        playlistService.current_playlist(playlist._id);
        //socket.join(playlists._id);

    };

    socket.on("videoAdded", function(data) {
        //console.log("reviced video", data);
        //alert(data)
        data.videoId = data.id.videoId;
        $scope.selectedPlaylist.videos.push(data)
    });

    socket.on("updateIndex", function(data) {
        var playlist = _.find($scope.playlists, function(playlist) {
            return playlist._id == data.playlistId;
        });
        $scope.selectVideo(playlist.videos[data.index], data.index)
    })

    $scope.addToPlaylist = function (video) {
        $scope.showSearched = false;
        //$scope.selectedPlaylist.videos.push(video);
        //$scope.playlists[$scope.selectedPlaylist.playlistIndex].videos.push(video)
        playlistService.add(video, $scope.selectedPlaylist._id).then(function(res) {
            $scope.selectedPlaylist.videos.push(res.data);
            console.log(video, "add video", res.data);
            socket.emit('addToPlaylist', {playlistId: $scope.selectedPlaylist._id, video: video});
            //$scope.playlists[$scope.selectedPlaylist.playlistIndex].videos.push(video)
        })
    };

    $scope.sharePlaylist = function(){
        playlistService.share($scope.selectedPlaylist._id).then(function(res) {
            $scope.selectedPlaylist.share_link = "/playlists/"+$scope.selectedPlaylist._id
        })
    };

    $scope.selectSuggested = function(index) {
        $scope.suggestedIndex = -1;
        $scope.search($scope.suggesteds[index][0])
    };

    $scope.deleteFromPlaylist = function(index) {
        console.log("call delete");
        $scope.selectedPlaylist.videos.splice(index, 1);
        socket.emit("deleteVideo", {
            playlistId: $scope.selectedPlaylist._id,
            index: index
        })
    };

    $scope.copyLink = function(){
        Copied = holdtext.createTextRange();
        Copied.execCommand("Copy");
    };

    $scope.deletePlaylist = function(id) {
        playlistService.deletePlaylist(id).then(function(res) {
            console.log(res, "data from delete");
            $scope.playlists = $scope.playlists.filter(function(v) {
                return v._id != res.data
            })
        })
    }

    function getId(video) {

        return video.id ? video.id.videoId : video.videoId
    }

    function getRandomInt(min, max) {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }

}]);

angular.module("youtube").controller("loginCtrl", ["$scope", "$http", function($scope, $http) {
    $scope.signIn = function(){

    }
}]);

angular.module("youtube").directive("ng-form", function(){
    var directive = {};
    directive.restrict = "E";
    directive.templateUrl = "/templates/form.html";
    directive.scope = {
        item: "="
    };
    return directive
});



//var tag = document.createElement('script');
//
//tag.src = "https://www.youtube.com/iframe_api";
//var firstScriptTag = document.getElementsByTagName('script')[0];
//firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

// 3. This function creates an <iframe> (and YouTube player)
//    after the API code downloads.
//var player;
//function onYouTubeIframeAPIReady() {
//    player = new YT.Player('player', {
//        height: '390',
//        width: '640',
//        videoId: 'M7lc1UVf-VE'
//    });
//    console.log(player);
//}
var API_KEY = "AIzaSyBhTNXcEHGkySSF3IrE97Bevsq-DBN_QYw"