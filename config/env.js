var env = {
    BASE_URL: "http://localhost:3000"
};

if(process.env.NODE_ENV == "staging"){
    env.BASE_URL = "https://youtube-music.herokuapp.com"
}
if(process.env.NODE_ENV == "production"){
    env.BASE_URL = "http://www.youtube-music.in"
}

module.exports = env;
