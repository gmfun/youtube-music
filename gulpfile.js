var gulp  = require('gulp'),
    gutil = require('gulp-util'),
    concat = require('gulp-concat'),
    autoprefixer = require('gulp-autoprefixer'),
    plumber = require('gulp-plumber')
less = require('gulp-less'),
    sourcemaps = require('gulp-sourcemaps');

// create a default task
gulp.task('default', ['concat', 'autoprefixer', 'watch', 'stylewatch']);

gulp.task('concat', function() {
    return gulp.src('js/*.js')
        .pipe(concat('all.js'))
        .pipe(gulp.dest('public/javascripts'));
});

gulp.task('watch', function () {
    return gulp.watch('js/*.js', ['concat'])
});

gulp.task('autoprefixer', function() {
    return gulp.src('css/style.less')
        .pipe(plumber())
        .pipe(sourcemaps.init())
        .pipe(less())
        .pipe(autoprefixer({
            browsers: ['last 2 versions'],
            cascade: false
        }))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest('public/stylesheets/'));
});

gulp.task('stylewatch', function(){
    return gulp.watch('css/*.less', [autoprefixer])
});