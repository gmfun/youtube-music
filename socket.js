var socketio = require('socket.io'),
    io, clients = {};
var Playlist = require('./model/playlist');
var onPlaylist = {}

module.exports = {

    startSocketServer: function (app) {
        io = socketio.listen(app);

        io.sockets.on('connection', function (socket) {
            console.log("new connection: " + socket.id);

            //socket.on('startplaylist',function(data) {
            //    console.log("socket data", data);
            //    onPlaylist[data.userId] = [
            //
            //    ]
            //})

            socket.on('addToPlaylist', function(data) {
                console.log("socket data", data);
                socket.to(data.playlistId).emit("videoAdded", data.video);
                //socket.emit("videoAdd", data.video)
            });

            socket.on('join', function(data) {
                console.log("asdked to join", data);
                socket.join(data.id)
            });

            socket.on("updateIndex", function(data) {
                console.log("data index", data);
                Playlist.findById(data.playlistId).then(function(playlist) {
                   playlist.current_index = data.index;
                   playlist.save().then(function(playlist) {
                       socket.to(playlist._id).emit("updateIndex", data)
                   })
               })
            });

            socket.on("deleteVideo", function(data) {
                Playlist.findById(data.playlistId).then(function(playlist) {
                    playlist.videos.splice(data.index, 1)
                    playlist.save().then(function(){
                        console.log("asked to delete", data);
                        socket.to(data.playlistId).emit("videoDeleted", data)
                    })
                })
            })

            socket.on('disconnect', function () {
                console.log("device disconnected");

            });

            //socket.on('connect_device', function (data, fn) {
            //    console.log("data from connected device: " + data);
            //    for (var col in data) {
            //        console.log(col + " => " + data[col]);
            //    }
            //
            //
            //});
        });
    }
};